package gui;
import java.io.*;
import sun.audio.*;

public class SoundPlayer {
	  public static void play() throws Exception {
	    // open the sound file as a Java input stream
	    String track = "assets/sounds/titel_full.wav";
	    InputStream in = new FileInputStream(track);

	    // create an audiostream from the inputstream
	    AudioStream audioStream = new AudioStream(in);

	    // play the audio clip with the audioplayer class
	    AudioPlayer.player.start(audioStream);
	  }
}
