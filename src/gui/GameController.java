package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Game;
import models.GameBoard;

import java.awt.Window.Type;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;
import javax.swing.JTextPane;
import java.awt.Font;

public class GameController extends JFrame {

	private JPanel contentPane;
	JTextPane txtMessage = new JTextPane();

	
	public GameController(GameBoard gb) {
		GameBoard targetBoard = gb;
		setResizable(false);
		setAlwaysOnTop(true);
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 142, 393);
		contentPane = new JPanel();
		contentPane.setForeground(Color.GREEN);
		contentPane.setBackground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		

		
		JButton btnMove = new JButton("Move");
		btnMove.setEnabled(false);
		btnMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.play();
			}
		});
		btnMove.setBounds(12, 258, 117, 29);
		contentPane.add(btnMove);
		
		JButton btnUndo = new JButton("Undo");
		btnUndo.setEnabled(false);
		btnUndo.setBounds(12, 299, 117, 29);
		contentPane.add(btnUndo);
		
		JToggleButton btnMode = new JToggleButton("Attack Mode");
		btnMode.setEnabled(false);
		btnMode.setBounds(12, 342, 117, 29);
		contentPane.add(btnMode);
		
		
		txtMessage.setFont(new Font("OCR A Std", Font.PLAIN, 14));
		txtMessage.setSelectedTextColor(Color.GREEN);
		txtMessage.setForeground(Color.GREEN);
		txtMessage.setBackground(Color.BLACK);
		txtMessage.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		txtMessage.setText("Welcome to Sky Wars. Commander, click 'Launch' to begin...");
		txtMessage.setBounds(12, 17, 117, 188);
		contentPane.add(txtMessage);
		
		
		JButton btnStart = new JButton("Launch");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMove.setEnabled(true);
				btnUndo.setEnabled(true);
				btnMode.setEnabled(true);
				btnStart.setEnabled(false);
				Game.launch();
				
			}
		});
		btnStart.setBounds(12, 217, 117, 29);
		contentPane.add(btnStart);
		
		
	}
	public void prompt(String theMessage) {
		txtMessage.setText(theMessage);
	}
}
