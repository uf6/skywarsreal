package logic;

import java.awt.Button;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import models.Game;
import models.GameBoard;
import models.Sky;
import gui.GameController;
import gui.SoundPlayer;


public class AppMain {

	static String title = "Sky Wars!";
	static Game theGame;
	static GameController gc;
	
	public static void main(String[] args) {
		new Thread(
				new Runnable() {
	                public void run() {
	                	try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	                    try {
	                        SoundPlayer.play();
	                    } catch (Exception e) {
	                        e.printStackTrace();
	                    }
	                }
	            }).start();
		
		SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
		         theGame = newGame();					// Game initialized, ready to start when player clicks 'Launch'
		         
		    }
		});
	}
	
	
	public static Game newGame() {
		
		JFrame frame = new JFrame(title);
		GameBoard gb = new GameBoard();
		Game theGame = new Game(gb);
		frame.add(gb);
		frame.pack();
		frame.setVisible(true);	
		return theGame;
	}
	
	// Strategy pattern: offence/defence switching?
	// Command pattern: undoing moves
	// Factory pattern: spawning new enemy ships												DONE
	// Observer pattern: Enemy ships observe master ship and receive updates on location		DONEZO!
	// Threads: Sound? Swing event thread?			
}
