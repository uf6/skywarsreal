package models;

public class BattleShooter extends Enemy {
	
	public BattleShooter() {
		this.shipType = 4;
		this.type = "bsh";
		this.avatar = "assets/img/battleshooter.png";
	}

}