package models;

public class BattleCruiser extends Enemy {
	
	public BattleCruiser() {
		this.shipType = 3;
		this.type = "bs";
		this.avatar = "assets/img/battlecruiser.png";
	}

}