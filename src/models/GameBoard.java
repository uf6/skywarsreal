package models;

import java.awt.GridLayout;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class GameBoard extends JPanel {

	private static int ROWS = 4;
	private static int COLS = 4;
	private static int MAX_SIZE = ROWS * COLS;
	public static int[] gameBoardArray;
	private static final int BLACKHOLE = 0;

	
	public GameBoard() {
		gameBoardArray = new int[MAX_SIZE];
		Arrays.fill(gameBoardArray, 0);
		GridLayout grid = new GridLayout(ROWS, COLS);
		grid.setHgap(0);
		grid.setVgap(0);
		this.setLayout(grid);
	}
	
	public void update(List<SpaceShip> ships) {
		Arrays.fill(gameBoardArray, 0);
		for (SpaceShip s : ships) {
			int currentSquareType = gameBoardArray[s.getCurrentPosition()];
			// if the current square is empty just set the tile type
			// to the current ship's ship type 
			if (currentSquareType == 0) {
				gameBoardArray[s.getCurrentPosition()] = s.getShipType();
			} else 
				// If this ship is a MasterSpaceShip...
				 if (s.getShipType() == 1) {
					 int newSquareType = 0;
					 switch (currentSquareType) {
					 	case 2:
					 	case 3:
					 	case 4:
					 		newSquareType = 7;
					 		break;
					 	case 7:
					 		newSquareType = 8;
					 		break;
					 	case 8:
					 		newSquareType = 9;
					 		break;
					 	case 9:
					 		newSquareType = 10;
					 		break;
					 }
				 gameBoardArray[s.getCurrentPosition()] = newSquareType;
			} else {
				// If this ship is an Enemy...
				int newSquareType = 0;
				 switch (currentSquareType) {
				 	case 1:
				 		newSquareType = 7;
				 		break;
				 	case 7:
				 		newSquareType = 8;
				 		break;
				 	case 8:
				 		newSquareType = 9;
				 		break;
				 	case 9:
				 		newSquareType = 10;
				 		break;
				 	case 2:
				 	case 3:
				 	case 4:
				 		newSquareType = 5;
				 		break;
				 	case 5:
				 		newSquareType = 6;
				 		break;	
				 }
			 gameBoardArray[s.getCurrentPosition()] = newSquareType;
			}
		}
		redraw();
	}

	public void redraw() {
		this.removeAll();
		for (int i = 0; i < MAX_SIZE; i++) {
			int tileType = gameBoardArray[i];
			this.add(new Tile(tileType));
		}
		this.validate();
	}

	public static int getROWS() {
		return ROWS;
	}

	public static int getCOLS() {
		return COLS;
	}

	public static int getMAX_SIZE() {
		return MAX_SIZE;
	}

	public static int[] getGameBoardArray() {
		return gameBoardArray;
	}

	public static int getBlackhole() {
		return BLACKHOLE;
	}
}
