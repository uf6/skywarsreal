package models;

public interface Observer {
	public void update(int position);

}
