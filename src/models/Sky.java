package models;

import java.awt.Button;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Sky extends JPanel {
	private static final int ROWS = 4;
	private static final int COLS = 4;
	private static final int MAX_SIZE = ROWS * COLS;
	private static final int BLACKHOLE = 0;
	private static int[] boardArray = new int[MAX_SIZE];
	
	static JPanel theBoard;
	static ImageIcon space = new ImageIcon("assets/img/space_blank.png");

	
	public Sky() {
		theBoard = new JPanel();
		
	}
	public static void draw(Map<String, Integer> theShips) {
		
	}
	
	public void init() {
		for (int y = 0; y < COLS; y++) {
			for (int x = 0; x < ROWS; x++) {
				theBoard.add(new JButton(space));
			}
		}
	}
	
	
	public static int getRows() {
		return ROWS;
	}
	public static int getCols() {
		return COLS;
	}
	public static int getMaxSize() {
		return MAX_SIZE;
	}
	public static int getBlackhole() {
		return BLACKHOLE;
	}
}
