package models;

import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Tile extends JButton {
	static ImageIcon empty = new ImageIcon("assets/img/space_blank.png");
	static ImageIcon masterShip = new ImageIcon("assets/img/mastership.png");
	static ImageIcon battleStar = new ImageIcon("assets/img/battleStar.png");
	static ImageIcon battleCruiser = new ImageIcon("assets/img/battleCruiser.png");
	static ImageIcon battleShooter = new ImageIcon("assets/img/battleShooter.png");
	
	static ImageIcon twoEnemies = new ImageIcon("assets/img/2enemies.png");
	static ImageIcon threeEnemies = new ImageIcon("assets/img/3enemies.png");
	
	static ImageIcon master1 = new ImageIcon("assets/img/masterand1.png");
	static ImageIcon master2 = new ImageIcon("assets/img/masterand2.png");
	static ImageIcon master3 = new ImageIcon("assets/img/masterand3.png");
	
	static ImageIcon explode = new ImageIcon("assets/img/explosion.png");
	
	public Tile(int tileType) {
		ImageIcon theIcon;
		
		switch(tileType) {
			case 1: theIcon = masterShip;
			killBorders();
			break;
			
			case 2: theIcon = battleStar;
			killBorders();
			break;
			
			case 3: theIcon = battleCruiser;
			killBorders();
			break;
			
			case 4: theIcon = battleShooter;
			killBorders();
			break;
			
			case 5: theIcon = twoEnemies;
			killBorders();
			break;
			
			case 6: theIcon = threeEnemies;
			killBorders();
			break;
			
			case 7: theIcon = master1;
			killBorders();
			break;
			
			case 8: theIcon = master2;
			killBorders();
			break;
			
			case 9: theIcon = master3;
			killBorders();
			break;
			
			case 10: theIcon = explode;
			killBorders();
			break;
			
			default: theIcon = empty;
			killBorders();
			break;
		}
		this.setIcon(theIcon);
//		this.repaint();
	}
	
	private void killBorders() {
		this.setBorder(null);
		this.setBorderPainted(false);
		this.setMargin(new Insets(0,0,0,0));
	}

}
