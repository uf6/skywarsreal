package models;

import java.util.ArrayList;

public class MasterSpaceShip extends SpaceShip implements Observable {
	int mode;			// 0 = defence, 1 = offence
	private ArrayList<Enemy> myEnemies = new ArrayList<>();
	
	public MasterSpaceShip(int square) {
		shipType = 1;
		type = "ms";
		avatar = "assets/img/mastership.png";
		mode = 0;
		currentPosition = square;
	}
	
	public void registerObserver(Enemy e) {
		this.myEnemies.add(e);
	}
	
	public void notifyObservers() {
		for(Enemy temp : myEnemies) {
			temp.update(getCurrentPosition());
		}
	}
}
