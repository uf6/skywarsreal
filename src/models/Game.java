package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import gui.GameController;

public class Game {

	static Random rand = new Random();
	static GameBoard board;
	static MasterSpaceShip player;
	static Stack<Move> theMoves;
	static Map<String, Integer> ships = new HashMap<>();
	static Map<Integer,Integer> enemies = new HashMap<>();
	
	static ArrayList<SpaceShip> spaceShips = new ArrayList<>();
	
	static int position;
	static GameController gc;
	
	static EnemyFactory theFactory;
	
	static final int UP = -4;
	static final int DOWN = 4;
	static final int LEFT = -1;
	static final int RIGHT = 1;
	static final int RUP = -3;
	static final int LUP = -5;
	static final int LDOWN = 3;
	static final int RDOWN = 5;

	public Game(GameBoard gb) {
		board = gb;
		board.redraw();
		
		gc = new GameController(gb);
		gc.prompt("Welcome to Sky Wars, Commander Jameson. Click 'Launch' to begin...");
		gc.setVisible(true);

	}

	public static void launch() {
		do {
			position = rand.nextInt(board.getMAX_SIZE());
		} while (position == board.getBlackhole());

		player = new MasterSpaceShip(position);
		spaceShips.add(player);
		board.update(spaceShips);
		System.out.println("In launch()");

	}

	public static void play() {
		/*********************** PLAYER'S MOVE *******************************/
		
		// Prompt player for move
		gc.prompt("Your move Commander...");				// FIXME make this appear only once Launch clicked
		
		// Calculate random position of mastership
		playerMove();
		
		// Spawn new enemy?
		if (spawnEnemy()) {
			theFactory = new EnemyFactory();
			Enemy enemy = theFactory.createEnemy();
			spaceShips.add(enemy);
			player.registerObserver(enemy);
		}
		
		// update UI
		System.out.println(spaceShips.toString());
		board.update(spaceShips);

		
		/*********************** END PLAYER'S MOVE *******************************/
		
		
		// 	UPDATE GAME STATE
		
		
		/*********************** ENEMIES' MOVES *******************************/
		
		
		if (!spaceShips.isEmpty()) {
			
			for (SpaceShip s : spaceShips) {
				if (s.getShipType() != 1) {
					Enemy e = (Enemy) s;
					if (e.getCycleCount() > 0) {
						enemyMove(e);
					}
				}
				s.cycleCount++;
				board.update(spaceShips);
			}
		}	
		
		/*********************** END ENEMIES' MOVES *******************************/
		
		
		// UPDATE GAME STATE
		
		
		// conflicts?
		
		
		
		
		// update state and push to moves stack
//		ships.remove(player.type);
//		ships.put(player.type, position);
		
		
		
		// update UI		
		
		
		// update isRunning?
		
		
		// Update enemies (observers)...
		player.notifyObservers();

	} ///////////////////////////////////////////// end play() ////////////////////////////////////////////////////
	
	public static void enemyMove(Enemy e) {
		int nextPos = rand.nextInt(board.getMAX_SIZE());
		while (nextPos == 0 || nextPos <= e.getUpdate() + 1 && nextPos > board.getMAX_SIZE() - 1) {
			nextPos = rand.nextInt(board.getMAX_SIZE());
		}
		e.move(nextPos);
	}
	
	
	public static boolean spawnEnemy() {
		// 1 in 3 chance of spawning a new enemy...
		int chance = rand.nextInt(2);
		if (chance == 0) {
			return true;
		}
		return false;
	}
	
	
	public static void playerMove() {
		
		int nextMove;
		do {
			nextMove = rand.nextInt(8);
			switch (nextMove) {
				case 0: nextMove = UP;
				break;
				case 1: nextMove = DOWN;
				break;
				case 2: nextMove = LEFT;
				break;
				case 3: nextMove = RIGHT;
				break;
				case 4: nextMove = RUP;
				break;
				case 5: nextMove = LUP;
				break;
				case 6: nextMove = LDOWN;
				break;
				case 7: nextMove = RDOWN;
				break;
			}
		} while ((player.getCurrentPosition() + nextMove) < 1
				|| (player.getCurrentPosition() + nextMove) > board.getMAX_SIZE() - 1);
		
		player.move(player.getCurrentPosition() + nextMove);
		System.out.println(player.getCurrentPosition());
	}
}
