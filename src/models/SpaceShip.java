package models;

public class SpaceShip {
	int currentPosition;
	int prevPosition;
	String type;
	int shipType; 
	String avatar;
	public int cycleCount;

	
	public void setShipType(int type) {
		this.shipType = type;
	}
	
	public int getShipType() {
		return shipType;
	}
	
	public void move(int pos) {
		this.prevPosition = this.currentPosition;
		this.currentPosition = pos;
	}
	
	public int getCurrentPosition() {
		return currentPosition;
	}
	
	public int getPreviousPosition() {
		return prevPosition;
	}
}


