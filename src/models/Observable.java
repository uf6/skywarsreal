package models;

public interface Observable {
	public void registerObserver(Enemy e);
	public void notifyObservers();
}
