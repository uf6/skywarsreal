package models;

import java.util.Random;

public class EnemyFactory extends Enemy {

	public Enemy createEnemy() {
		Enemy enemy = null;
		Random rand = new Random();
		int enemyType = 0;

		do {
			enemyType = rand.nextInt(5);
		} while (enemyType == 0 || enemyType == 1);
		
		switch(enemyType) {
			case 2: enemy = new BattleStar();
			enemy.setShipType(2);
			break;
			case 3: enemy = new BattleCruiser();
			enemy.setShipType(3);
			break;
			case 4: enemy = new BattleShooter();
			enemy.setShipType(4);
			break;
		}

		return enemy;
	}
}
