package models;

import java.util.Random;

public abstract class Enemy extends SpaceShip implements Observer {
	
	Random rand = new Random();
	int targetUpdates;
	
	public Enemy() {
		currentPosition = 0;
//		prevPosition = 0;
		cycleCount = 0;
	}

	public int getCycleCount() {
		return cycleCount;
	}

	public void setCycleCount(int cycleCount) {
		this.cycleCount = cycleCount;
	}
	
	public void update(int update) {
		this.targetUpdates = update;
	}
	
	public int getUpdate() {
		return targetUpdates;
	}
}
